 let map;

 function initMap()
 {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 43.7269,
            lng: 4.07884
        },
        zoom: 3
    });
 }
 
export {initMap};